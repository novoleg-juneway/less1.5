FROM debian:10 as builder

RUN apt update && apt install -y ca-certificates curl gnupg lsb-release curl

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt update && apt install -y docker-ce docker-ce-cli containerd.io


FROM debian:10-slim as runner

RUN apt update && apt install -y iptables curl procps net-tools strace && \
apt clean -y && \
apt autoclean -y && \
rm -rf var/cache/*

COPY ./start.sh .
COPY --from=builder /usr/bin/docker* /usr/bin/
COPY --from=builder /usr/bin/containerd* /usr/bin/
COPY --from=builder /usr/bin/runc /usr/bin/
COPY --from=builder /lib/x86_64-linux-gnu/libdevmapper.so.1.02.1 /lib
COPY --from=builder /etc/ssl/certs/ /etc/ssl/certs/
CMD ["/bin/bash","start.sh"]
