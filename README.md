# less1.5

```
# сборка кастомного docker образа dind на основе debian
docker build -t mydind .

# запуск кастомного контейнера dind на основе debian
docker run --privileged -v "$(pwd)"/nginx.conf:/home/nginx.conf -v "$(pwd)"/Dockerfile.into:/home/Dockerfile.into -d mydind

# сборка nginx внутри кастомного контейнера
docker build -t nginx -f Dockerfile.into myind .

# запуск nginx внутри кастомного контейнера
docker run -v "$(pwd)"/nginx.conf:/etc/nginx/nginx.conf \
  -p 80:8080 -d --name nginx nginx
```